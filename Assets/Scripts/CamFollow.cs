﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour {

    public Vector3 offsetPosition;
    public Transform target;

    private Space offsetPositionSpace = Space.Self;
    private bool lookAt = true;

    public float currentY = 0.0f;
    public float sensitivityY = 1.0f;

    public void Update()
    {
        if (Input.GetAxis("RStickH1") > .5f)
        {
            currentY += sensitivityY;
            //lookAt = false;
        }
        if (Input.GetAxis("RStickH1") < -.5f)
        {
            currentY -= sensitivityY;
            //lookAt = false;
        }
        else
        {
            lookAt = true;
        }
    }

    private void LateUpdate()
    {
        Refresh();
    }

    private void Refresh()
    {
        // compute position
        if (offsetPositionSpace == Space.Self)
        {
            transform.position = target.TransformPoint(offsetPosition);
        }
        else
        {
            transform.position = target.position + offsetPosition;
        }

        // compute rotation
        if (lookAt)
        {
            transform.LookAt(target);
        }
        else
        {
            //transform.rotation = Quaternion.Euler(0, currentY, 0);
            transform.rotation = target.rotation;
        }
    }
}
