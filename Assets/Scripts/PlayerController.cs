﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public float speed;
    public float turnSpeed;

	// Use this for initialization
	void Start () {
	}

    void FixedUpdate()
    {
        controlPlayer();  
    }

    // Update is called once per frame
    void Update () {
		
	}

    void controlPlayer()
    {

        //float moveHorizontal = Input.GetAxis("Horizontal");
        
        float acceleration = speed * Time.deltaTime;
        float turnAccel = turnSpeed * Time.deltaTime;

        if (Input.GetAxis("RTrigger1") == 1)
        {
            transform.Translate(Vector3.forward * acceleration);
        }

        if (Input.GetAxis("LTrigger1") == 1)
        {
            transform.Translate(Vector3.back * acceleration);
        }


        if (Input.GetAxis("LStickH1") > .5f)
        {
            transform.Rotate(Vector3.up * turnAccel);
        }

        if (Input.GetAxis("LStickH1") < -.5f)
        {
            transform.Rotate(Vector3.down * turnAccel);
        }


        /*
        
        

        Vector3 movement = new Vector3(moveHorizontal * acceleration, 0, moveVertical * acceleration);
        rid.MovePosition(transform.position + movement);*/
    }
}
