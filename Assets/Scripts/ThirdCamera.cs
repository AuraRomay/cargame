﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdCamera : MonoBehaviour {
    //private const int Y_ANGLE_MIN = -50;
    //private const int Y_ANGLE_MAX = 50;
    

    public Transform lookAt;
    public Transform camTrans;

    private Camera cam;

    //private float distance = 10.0f;
   
    public float currentY = 0.0f;
  
    public float sensitivityY = 1.0f;

    // Use this for initialization
    void Start () {
        camTrans = transform;
        cam = Camera.main;
	}

    void Update () {
     
        if (Input.GetAxis("RStickH1") > .5f)
        {
            Debug.Log("fghj");
            currentY += sensitivityY;
        }
        if (Input.GetAxis("RStickH1") < -.5f)
        {
            currentY -= sensitivityY;
        }

        currentY = Mathf.Clamp(currentY, -50, 50);

    }

    void LateUpdate()
    {
        Vector3 direction = new Vector3(-0.15f, 0, -2.53f);
        Quaternion rotation = Quaternion.Euler(25, currentY, 0);
        camTrans.position = lookAt.position + rotation * direction;
        camTrans.LookAt(cam.transform.position);
    }

    // Update is called once per frame
}
